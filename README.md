#### 프로젝트 초기화
저장소 복제 후 최초 한번 프로젝트를 초기화 한다.
```bash
npm install express
```

# 사용자 관리 (CRUD) API
> 사용자를 신규로 생성, 조회, 변경, 삭제를 하기 위한 API 기능을 구현하도록 한다.

- 디렉토리 구조
- Model and Validation
- 사용자 생성
- 사용자 조회
- 사용자 변경
- 사용자 삭제
- 신규 api 기능 적용


### 디렉토리 구조
사용자 관리 기능을 구현하기 위해 3개의 js파일이 필요하다. 먼저 user의 정보를 저장하기 위해 model을 정의한다. 모델에 데이터를 저장하기 전에 유효성을 체크하기 위한 로직을 validation 파일에 정의한다. user.js는 라우터로 api 기능을 정의하기 위한 파일이다.
```
./model/user.model.js
./validation/user.validation.js
./router/user.js
```

### Model and Validation
Model은 디비에 데이터를 저장하기 위한 용도록 사용하는 객체이다. 여기서는 mongoose를 이용하여 모델을 선언한다.

#### 1) Model
모델에는 저장되는 필드에 대해 필수여부, 길이, 타입 등을 선언할 수 있다. 여기서는 간단하게 이름, 이메일, 패스워드 등을 선언한다. 파일명은 패키지명에 해당하는 `user`를 prefix로하여 `user.model.js`로 생성한다. timestamp를 true로 설정하면 입력 날짜와 변경 날짜가 자동으로 저장하게 된다.
```javascript
const mongoose = require("mongoose");
const userSchemea  = new mongoose.Schema({
    name : {
        type: String,
        required: true,
        min: 6, 
        max: 255
    },
    email : {
        type : String, 
        required : true, 
        min : 6,
        max : 255
    },
    password : {
        type : String, 
        required : true,
        min : 6,
        max : 1024
    }
}, {
    timestamps  : true
});
```  

모듈을 생성을 완료 하였으면 export를 해준다. 
```javascript   
module.exports = mongoose.model('User', userSchemea);
```


#### 2) Validation
사용자로 부터 입력 받은 값을 디비에 저장하기 전에 반드시 유효성 체크를 하여 데이터의 품질을 높여 주도록 한다. 이를 구현하기 위해 Joi모듈을 이용한다. 파일은 `user.validation.js`로 만들어서 validation 디렉토리에 저장한다.

```javascript   
const Joi = require('joi');
```



### 사용자 등록
Model은 디비에 데이터를 저장하기 위한 용도록 사용하는 객체이다. 여기서는 mongoose를 이용하여 모델을 선언한다.


#### 1) Validation 추가
user.validation.js파일에 사용자 등록에 대한  validation 함수를 추가한다.
name, email, password 3개 필드에 대한 유효성을 검사하는 기능을 가진디ㅏ. email 의 경우 email()를 통해 이메일 형식인지를 확인하는 체크기능을 추가 정의한다.
```javascript   
const registerValidation = (data) => {

    const schema = {
        name: Joi.string().min(6).required(),
        email: Joi.string().min(6).required().email(),
        password: Joi.string().min(6).required()
    };
    return Joi.validate(data,schema);

};
```

추가된 함수를 export 한다.
```javascript   
module.exports.registerValidation = registerValidation;
```

#### 2) 사용자 등록 api
사용자 등록 api는 라우터에 user.js파일을 만들고 그 안에 메소드를 정의한다. 사용자를 등록하기에 앞서 먼저 유효성을 검사하는 로직을 정의한다. 실패시 더이상 메소드를 진행하지 않고 화면에 에러를 반환한다.   
유효성 검사가 완료되면 사용자가 존재하는지 디비에서 체크하는 로직을 추가한다. 이후 패스워드를 암호화 처리한 후 최종적으로 값을 디비에 저장한다.
```javascript   
router.post('/add', async (req, res) => {
    // 유효성 검사
    const { error } = registerValidation(req.body);
    if(error)  return res.status(400).send(error.details[0].message);

    // 기 등록된 계정 정보가 있는지 확인
    const emailExist = await User.findOne({email : req.body.email});
    if(emailExist) res.status(400).send('Email already exists');

    // 비밀번호 암호화 처리 
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password , salt);

    // regist a new user
    const user = new User({ // 몽고 디비에 저장하기 위해 스키마에 값을 바인딩 한다.
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword // 암호화된 값으로 변경 
    });
    
    try {
        // write code to connect to db
        const savedUser = await user.save(); //값 저장
        res.send({user : user._id}); // 등록된 문서 아이디 반환(고유값)
    } catch (err) {
        res.status(400).send(err);
    }
});
```



### 사용자 조회

#### 1) 사용자 전체 목록 조회
사용자 전체 목록을 조회하는 api이다. then() 을 이용하여 사용자를 조회한 후 결과를 사용자에게 반환한다.
```javascript   
router.get('/', (req, res) => {
    User.find()
    .then( users =>  res.json(users))
    .catch( err => res.status(400).json( 'error: '  + err ));
});
```

#### 2) 사용자 상세 조회
사용자 상세조회는 uri의 값을 인자로 받아 해당하는 사용자 정보를 조회한다. async, await 기능을 이용하여 사용자 상세 정보를 조회한다.   
```javascript   
router.get('/:id', async (req, res) => {
    const userExist = await User.findOne({ _id :  req.params.id });
     try {
         res.json(userExist); // 등록된 문서 아이디
     } catch (err) {
         res.status(400).send(err);
     }
});
```

### 사용자 변경
#### 1) 변경 validation 추가 
변경시에는 등록과 다른 유효성 기능을 갖게 된다. 본 예제에서는 비밀번호의 경우 별도의 로직에서 처리하기 위해서 제외하고 변경은 name, email만 허영하는 로직이다. validation은 2개의 항목만 추가하여 정의 한다.   
```javascript   
const userUpdateValidation = (data) => {
    const schema = {
        name: Joi.string().min(6).required(),
        email: Joi.string().min(6).required().email()
    };
    return Joi.validate(data,schema);
};
```
export를 맨 하단에 추가한다.
```javascript   
module.exports.userUpdateValidation = userUpdateValidation;
```

#### 2) 사용자 변경 api 
사용자 변경 api는 등록과 다른 validation 함수를 사용하게 된다. 몽고디비의 경우 RDBM과 처리하는 방법에 차이가 있다. 또한 mongoose 와 mongodb db api를 사용하는 법도 다소 차이가 존재 한다. 여기서는 mogoose의 기능을 이용한 변경 처리이다. 변경하는 값은 name과 email이고 나머지 정보를 그대로 유지해야 하기때문에 디비에 저장된 값을 읽어와 변경된 내용을 replace 한 뒤 저장하는 방법을 사용 하였다.    
저장은 updateOne() 메소드를 사용 하였는데 첫번째 인자값은 필터 정보이고 2번째 인자값은 변경하고자 하는 document이다. 3번째 에러 함수를 이용하지 않고 try~catch 구문내에 실행 코드를 정의하여 에러 발생을 핸들링 하도록 구현 하였다.   
```javascript   
router.put('/:id', async (req, res) => {
    // 유효성 검사
    const { error } = userUpdateValidation(req.body);
    if(error)  return res.status(400).send(error.details[0].message);
    
    // db에서 값 검색
    const idExist = await User.findOne({_id : req.params.id });

    // 값 변경
    idExist.name = req.body.name;
    idExist.email = req.body.email;

    try {
        // write code to connect to db
        const savedUser = await User.updateOne(
            { _id : req.params.id},  // key
            idExist                  // 변경 document
        );
        res.send(savedUser); // 처리 결과 전송
 
    } catch (err) {
        console.log('error : ' + err.message);
        res.status(400).send(err);
    }
});

```

### 사용자 삭제
사용자를 삭제하기 위해서 deleteOne함수를 이용한다. document의 object id 값을 이용하여 삭제처리 한다.

```javascript   
router.delete('/:id', async (req, res) => {
    const userExist = await User.deleteOne({ _id :  req.params.id });
     try {
         res.json(userExist); // 등록된 문서 아이디
     } catch (err) {
         res.status(400).send(err);
     }
});
```

### 신규 기능 적용하기
새롭게 추가된 api를 서버에 노출하기 위해서는 app.js 서버에 등록해 주어야 한다.


#### 1) user.js import
생성된 user.js파일을 app.js 파일에 import 한다.
```javascript   
const userRouter = require('./routes/user'); // user api
```
#### 2) app.use 적용
접근 주소(prefix)를 정의한 후 userRouter를 추가한다.
```javascript   
app.use("/api/users", userRouter);  // bbs api
```

